function createCard(name, description, pictureUrl, formattedStartDate, formattedEndDate, locationName) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${formattedStartDate} - ${formattedEndDate}</div>
    </div>
  `;
}

function errorMessage(message) {
  const html =`<div class="alert alert-danger" role="alert>
  message = ${message}
</div>`
  const error = document.querySelector('.error');
  error.innerHTML = html
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("bad url");
      errorMessage("Response was not OK")// Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details.conference);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const columns = document.querySelectorAll('.col');
          const startDate = new Date(details.conference.starts);
          const endDate = new Date(details.conference.ends);
          const formattedStartDate = startDate.toLocaleDateString();
          const formattedEndDate = endDate.toLocaleDateString();
          const locationName = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, formattedStartDate, formattedEndDate, locationName);
          console.log(columns);
          columns[0].innerHTML += html;
        }
      }

    }
  } catch (e) {
    console.log("ERROR", e.message);
    errorMessage(e);// Figure out what to do if an error is raised
  }

});
// var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
// var alertTrigger = document.getElementById('liveAlertBtn')

// function alert(message, type) {
//   var wrapper = document.createElement('div')
//   wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

//   alertPlaceholder.append(wrapper)
// }

// if (alertTrigger) {
//   alertTrigger.addEventListener('click', function () {
//     alert('Nice, you triggered this alert message!', 'success')
//   })
// }
